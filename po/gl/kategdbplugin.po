# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.es>, 2012, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2017.
# SPDX-FileCopyrightText: 2023 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-15 01:54+0000\n"
"PO-Revision-Date: 2023-10-18 08:25+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#. i18n: ectx: property (text), widget (QLabel, u_gdbLabel)
#: advanced_settings.ui:17
#, kde-format
msgid "GDB command"
msgstr "Orde de GDB"

#. i18n: ectx: property (text), widget (QToolButton, u_gdbBrowse)
#. i18n: ectx: property (text), widget (QToolButton, u_addSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSrcPath)
#. i18n: ectx: property (text), widget (QToolButton, u_setSoPrefix)
#. i18n: ectx: property (text), widget (QToolButton, u_addSoSearchPath)
#. i18n: ectx: property (text), widget (QToolButton, u_delSoSearchPath)
#: advanced_settings.ui:30 advanced_settings.ui:62 advanced_settings.ui:69
#: advanced_settings.ui:241 advanced_settings.ui:274 advanced_settings.ui:281
#, kde-format
msgid "..."
msgstr "…"

#. i18n: ectx: property (text), widget (QLabel, u_srcPathsLabel)
#: advanced_settings.ui:37
#, kde-format
msgid "Source file search paths"
msgstr "Rutas de busca de ficheiros fonte"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:92
#, kde-format
msgid "Local application"
msgstr "Aplicación local"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:97
#, kde-format
msgid "Remote TCP"
msgstr "TCP remoto"

#. i18n: ectx: property (text), item, widget (QComboBox, u_localRemote)
#: advanced_settings.ui:102
#, kde-format
msgid "Remote Serial Port"
msgstr "Porto serie remoto"

#. i18n: ectx: property (text), widget (QLabel, u_hostLabel)
#: advanced_settings.ui:127
#, kde-format
msgid "Host"
msgstr "Servidor"

#. i18n: ectx: property (text), widget (QLabel, u_tcpPortLabel)
#. i18n: ectx: property (text), widget (QLabel, u_ttyLabel)
#: advanced_settings.ui:141 advanced_settings.ui:166
#, kde-format
msgid "Port"
msgstr "Porto"

#. i18n: ectx: property (text), widget (QLabel, u_ttyBaudLabel)
#: advanced_settings.ui:183
#, kde-format
msgid "Baud"
msgstr "Baud"

#. i18n: ectx: property (text), widget (QLabel, u_soAbsPrefixLabel)
#: advanced_settings.ui:231
#, kde-format
msgid "solib-absolute-prefix"
msgstr "solib-absolute-prefix"

# skip-rule: trasno-library_reverse
#. i18n: ectx: property (text), widget (QLabel, u_soSearchLabel)
#: advanced_settings.ui:248
#, kde-format
msgid "solib-search-path"
msgstr "ruta-de-busca-de-bibliotecas-so"

#. i18n: ectx: property (title), widget (QGroupBox, u_customInitGB)
#: advanced_settings.ui:317
#, kde-format
msgid "Custom Init Commands"
msgstr "Ordes de inicialización personalizadas"

#: backend.cpp:24 backend.cpp:49 debugview_dap.cpp:155
#, kde-format
msgid ""
"A debugging session is on course. Please, use re-run or stop the current "
"session."
msgstr ""
"Hai unha sesión de depuración en marcha. Use a funcionalidade de executar de "
"novo ou deteña a sesión actual."

#: configview.cpp:92
#, kde-format
msgid "Add new target"
msgstr "Engadir un novo destino"

#: configview.cpp:96
#, kde-format
msgid "Copy target"
msgstr "Copiar o destino"

#: configview.cpp:100
#, kde-format
msgid "Delete target"
msgstr "Eliminar o destino"

#: configview.cpp:105
#, kde-format
msgid "Executable:"
msgstr "Executábel:"

#: configview.cpp:125
#, kde-format
msgid "Working Directory:"
msgstr "Cartafol de traballo:"

#: configview.cpp:133
#, kde-format
msgid "Process Id:"
msgstr "Identificador do proceso:"

#: configview.cpp:138
#, kde-format
msgctxt "Program argument list"
msgid "Arguments:"
msgstr "Argumentos:"

#: configview.cpp:141
#, kde-format
msgctxt "Checkbox to for keeping focus on the command line"
msgid "Keep focus"
msgstr "Manter o foco"

#: configview.cpp:142
#, kde-format
msgid "Keep the focus on the command line"
msgstr "Manter o foco na liña de ordes"

#: configview.cpp:144
#, kde-format
msgid "Redirect IO"
msgstr "Redirixir a E/S"

#: configview.cpp:145
#, kde-format
msgid "Redirect the debugged programs IO to a separate tab"
msgstr "Redirixir a E/S dos programas en depuración a outra lapela."

#: configview.cpp:147
#, kde-format
msgid "Advanced Settings"
msgstr "Configuración avanzada"

#: configview.cpp:231
#, kde-format
msgid "Targets"
msgstr "Destinos"

#: configview.cpp:524 configview.cpp:537
#, kde-format
msgid "Target %1"
msgstr "Destino %1"

#. i18n: ectx: attribute (title), widget (QWidget, tab_1)
#: debugconfig.ui:33
#, kde-format
msgid "User Debug Adapter Settings"
msgstr "Configuración do adaptador de depuración da persoa usuaria"

#. i18n: ectx: property (text), widget (QLabel, label)
#: debugconfig.ui:41
#, kde-format
msgid "Settings File:"
msgstr "Ficheiro de configuración:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: debugconfig.ui:68
#, kde-format
msgid "Default Debug Adapter Settings"
msgstr "Configuración do adaptador de depuración predeterminado"

#: debugconfigpage.cpp:72 debugconfigpage.cpp:77
#, kde-format
msgid "Debugger"
msgstr "Depurador"

#: debugconfigpage.cpp:128
#, kde-format
msgid "No JSON data to validate."
msgstr "Non hai datos JSON para validar."

#: debugconfigpage.cpp:136
#, kde-format
msgid "JSON data is valid."
msgstr "Os datos JSON son válidos."

#: debugconfigpage.cpp:138
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Os datos JSON non son válidos: non hai un obxecto JSON."

#: debugconfigpage.cpp:141
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Os datos JSON non son válidos: %1"

#: debugview.cpp:35
#, kde-format
msgid "Locals"
msgstr "Locais"

#: debugview.cpp:37
#, kde-format
msgid "CPU registers"
msgstr "Rexistros do procesador"

#: debugview.cpp:160
#, kde-format
msgid "Please set the executable in the 'Settings' tab in the 'Debug' panel."
msgstr ""
"Defina o executábel na lapela de «Configuración» do panel de «Depuración»."

#: debugview.cpp:169
#, kde-format
msgid ""
"No debugger selected. Please select one in the 'Settings' tab in the 'Debug' "
"panel."
msgstr ""
"Non seleccionou un depurador. Seleccione un na lapela «Configuración» do "
"panel «Depuración»."

#: debugview.cpp:178
#, kde-format
msgid ""
"Debugger not found. Please make sure you have it installed in your system. "
"The selected debugger is '%1'"
msgstr ""
"Non se atopou o depurador. Asegúrase de que o ten instalado no sistema. O "
"depurador seleccionado é «%1»."

#: debugview.cpp:384
#, kde-format
msgid "Could not start debugger process"
msgstr "Non foi posíbel iniciar o proceso do depurador"

#: debugview.cpp:442
#, kde-format
msgid "*** gdb exited normally ***"
msgstr "*** gdb saíu con normalidade ***"

#: debugview.cpp:648
#, kde-format
msgid "all threads running"
msgstr "todos os fíos en execución"

#: debugview.cpp:650
#, kde-format
msgid "thread(s) running: %1"
msgstr "fío(s) en execución: %1"

#: debugview.cpp:655 debugview_dap.cpp:270
#, kde-format
msgid "stopped (%1)."
msgstr "detidos (%1)."

#: debugview.cpp:659 debugview_dap.cpp:278
#, kde-format
msgid "Active thread: %1 (all threads stopped)."
msgstr "Fío activo: %1 (detivéronse todos os fíos)."

#: debugview.cpp:661 debugview_dap.cpp:280
#, kde-format
msgid "Active thread: %1."
msgstr "Fío activo: %1."

#: debugview.cpp:680
#, kde-format
msgid "Current frame: %1:%2"
msgstr "Chamada actual: %1:%2"

#: debugview.cpp:707
#, kde-format
msgid "Host: %1. Target: %1"
msgstr "Máquina: %1. Obxectivo: %1."

#: debugview.cpp:1377
#, kde-format
msgid ""
"gdb-mi: Could not parse last response: %1. Too many consecutive errors. "
"Shutting down."
msgstr ""
"gdb-mi: Non foi posíbel analizar a última resposta: %1. Demasiados erros "
"consecutivos. Apagarase."

#: debugview.cpp:1379
#, kde-format
msgid "gdb-mi: Could not parse last response: %1"
msgstr "gdb-mi: Non foi posíbel analizar a última resposta: %1."

#: debugview_dap.cpp:169
#, kde-format
msgid "DAP backend failed"
msgstr "A infraestrutura de DAP fallou."

#: debugview_dap.cpp:211
#, kde-format
msgid "program terminated"
msgstr "o programa terminou"

#: debugview_dap.cpp:223
#, kde-format
msgid "requesting disconnection"
msgstr "solicitando a desconexión"

#: debugview_dap.cpp:237
#, kde-format
msgid "requesting shutdown"
msgstr "solicitando o apagado"

#: debugview_dap.cpp:261
#, kde-format
msgid "DAP backend: %1"
msgstr "Infraestrutura de DAP: %1"

#: debugview_dap.cpp:285
#, kde-format
msgid "Breakpoint(s) reached:"
msgstr "Punto(s) de parada acadados:"

#: debugview_dap.cpp:307
#, kde-format
msgid "(continued) thread %1"
msgstr "fío (continuado) %1"

#: debugview_dap.cpp:309
#, kde-format
msgid "all threads continued"
msgstr "todos os fíos continuados"

#: debugview_dap.cpp:316
#, kde-format
msgid "(running)"
msgstr "(en execución)"

#: debugview_dap.cpp:404
#, kde-format
msgid "*** connection with server closed ***"
msgstr "*** pechouse a conexión co servidor ***"

#: debugview_dap.cpp:411
#, kde-format
msgid "program exited with code %1"
msgstr "o programa saíu co código %1"

#: debugview_dap.cpp:425
#, kde-format
msgid "*** waiting for user actions ***"
msgstr "*** agardando por accións por parte da persoa usuaria ***"

#: debugview_dap.cpp:430
#, kde-format
msgid "error on response: %1"
msgstr "erro na resposta: %1"

#: debugview_dap.cpp:445
#, kde-format
msgid "important"
msgstr "importante"

#: debugview_dap.cpp:448
#, kde-format
msgid "telemetry"
msgstr "telemetría"

#: debugview_dap.cpp:467
#, kde-format
msgid "debugging process [%1] %2"
msgstr "depurando o proceso [%1] %2"

#: debugview_dap.cpp:469
#, kde-format
msgid "debugging process %1"
msgstr "depurando o proceso %1"

#: debugview_dap.cpp:472
#, kde-format
msgid "Start method: %1"
msgstr "Método de inicio: %1"

#: debugview_dap.cpp:479
#, kde-format
msgid "thread %1"
msgstr "fío %1"

#: debugview_dap.cpp:633
#, kde-format
msgid "breakpoint set"
msgstr "definiuse o punto de parada"

#: debugview_dap.cpp:641
#, kde-format
msgid "breakpoint cleared"
msgstr "borrouse o punto de parada"

#: debugview_dap.cpp:700
#, kde-format
msgid "(%1) breakpoint"
msgstr "(%1) punto de parada"

#: debugview_dap.cpp:717
#, kde-format
msgid "<not evaluated>"
msgstr "<non se avaliou>"

#: debugview_dap.cpp:739
#, kde-format
msgid "server capabilities"
msgstr "capacidades do servidor"

#: debugview_dap.cpp:742
#, kde-format
msgid "supported"
msgstr "permitido"

#: debugview_dap.cpp:742
#, kde-format
msgid "unsupported"
msgstr "non permitido"

#: debugview_dap.cpp:745
#, kde-format
msgid "conditional breakpoints"
msgstr "puntos de parada condicionais"

#: debugview_dap.cpp:746
#, kde-format
msgid "function breakpoints"
msgstr "puntos de parada de funcións"

#: debugview_dap.cpp:747
#, kde-format
msgid "hit conditional breakpoints"
msgstr "puntos de parada segundo se acaden"

#: debugview_dap.cpp:748
#, kde-format
msgid "log points"
msgstr "puntos de rexistro"

#: debugview_dap.cpp:748
#, kde-format
msgid "modules request"
msgstr "solicitude de módulos"

#: debugview_dap.cpp:749
#, kde-format
msgid "goto targets request"
msgstr "solicitude de destinos de «goto»"

#: debugview_dap.cpp:750
#, kde-format
msgid "terminate request"
msgstr "terminar a solicitude"

#: debugview_dap.cpp:751
#, kde-format
msgid "terminate debuggee"
msgstr "terminar o depurado"

#: debugview_dap.cpp:958
#, kde-format
msgid "syntax error: expression not found"
msgstr "erro de sintaxe: non se atopou a expresión"

#: debugview_dap.cpp:976 debugview_dap.cpp:1011 debugview_dap.cpp:1049
#: debugview_dap.cpp:1083 debugview_dap.cpp:1119 debugview_dap.cpp:1155
#: debugview_dap.cpp:1191 debugview_dap.cpp:1291 debugview_dap.cpp:1353
#, kde-format
msgid "syntax error: %1"
msgstr "erro de sintaxe: %1"

#: debugview_dap.cpp:984 debugview_dap.cpp:1019 debugview_dap.cpp:1298
#: debugview_dap.cpp:1361
#, kde-format
msgid "invalid line: %1"
msgstr "liña incorrecta: %1"

#: debugview_dap.cpp:991 debugview_dap.cpp:996 debugview_dap.cpp:1026
#: debugview_dap.cpp:1031 debugview_dap.cpp:1322 debugview_dap.cpp:1327
#: debugview_dap.cpp:1368 debugview_dap.cpp:1373
#, kde-format
msgid "file not specified: %1"
msgstr "non se indicou un ficheiro: %1"

#: debugview_dap.cpp:1061 debugview_dap.cpp:1095 debugview_dap.cpp:1131
#: debugview_dap.cpp:1167 debugview_dap.cpp:1203
#, kde-format
msgid "invalid thread id: %1"
msgstr "o identificador de fío non é válido: %1"

#: debugview_dap.cpp:1067 debugview_dap.cpp:1101 debugview_dap.cpp:1137
#: debugview_dap.cpp:1173 debugview_dap.cpp:1209
#, kde-format
msgid "thread id not specified: %1"
msgstr "non se indicou un identificador de fío: %1"

#: debugview_dap.cpp:1220
#, kde-format
msgid "Available commands:"
msgstr "Ordes dispoñíbeis:"

#: debugview_dap.cpp:1308
#, kde-format
msgid "conditional breakpoints are not supported by the server"
msgstr "o servidor non permite puntos de parada condicionais"

#: debugview_dap.cpp:1316
#, kde-format
msgid "hit conditional breakpoints are not supported by the server"
msgstr "o servidor non permite puntos de parada segundo se acaden"

#: debugview_dap.cpp:1336
#, kde-format
msgid "line %1 already has a breakpoint"
msgstr "a liña %1 xa ten un punto de parada"

#: debugview_dap.cpp:1381
#, kde-format
msgid "breakpoint not found (%1:%2)"
msgstr "non se atopou o punto de parada (%1:%2)"

#: debugview_dap.cpp:1387
#, kde-format
msgid "Current thread: "
msgstr "Fío actual: "

#: debugview_dap.cpp:1392 debugview_dap.cpp:1399 debugview_dap.cpp:1423
#, kde-format
msgid "none"
msgstr "ningún"

#: debugview_dap.cpp:1395
#, kde-format
msgid "Current frame: "
msgstr "Chamada actual: "

#: debugview_dap.cpp:1402
#, kde-format
msgid "Session state: "
msgstr "Estado da sesión: "

#: debugview_dap.cpp:1405
#, kde-format
msgid "initializing"
msgstr "inicializando"

#: debugview_dap.cpp:1408
#, kde-format
msgid "running"
msgstr "en execución"

#: debugview_dap.cpp:1411
#, kde-format
msgid "stopped"
msgstr "detido"

#: debugview_dap.cpp:1414
#, kde-format
msgid "terminated"
msgstr "terminado"

#: debugview_dap.cpp:1417
#, kde-format
msgid "disconnected"
msgstr "desconectado"

#: debugview_dap.cpp:1420
#, kde-format
msgid "post mortem"
msgstr "autopsia"

#: debugview_dap.cpp:1476
#, kde-format
msgid "command not found"
msgstr "non se atopou unha orde"

#: debugview_dap.cpp:1497
#, kde-format
msgid "missing thread id"
msgstr "falta o identificador de fío"

#: debugview_dap.cpp:1605
#, kde-format
msgid "killing backend"
msgstr "matando a infraestrutura"

#: debugview_dap.cpp:1663
#, kde-format
msgid "Current frame [%3]: %1:%2 (%4)"
msgstr "Chamada actual [%3]: %1:%2 (%4)"

#: localsview.cpp:17
#, kde-format
msgid "Symbol"
msgstr "Símbolo"

#: localsview.cpp:18
#, kde-format
msgid "Value"
msgstr "Valor"

#: localsview.cpp:41
#, kde-format
msgid "type"
msgstr "tipo"

#: localsview.cpp:50
#, kde-format
msgid "indexed items"
msgstr "elementos indexados"

#: localsview.cpp:53
#, kde-format
msgid "named items"
msgstr "elementos nomeados"

#: plugin_kategdb.cpp:103
#, kde-format
msgid "Kate Debug"
msgstr "Depuración de Kate"

#: plugin_kategdb.cpp:107
#, kde-format
msgid "Debug View"
msgstr "Vista de depuración"

#: plugin_kategdb.cpp:107 plugin_kategdb.cpp:340
#, kde-format
msgid "Debug"
msgstr "Depurar"

#: plugin_kategdb.cpp:110 plugin_kategdb.cpp:113
#, kde-format
msgid "Locals and Stack"
msgstr "Locais e pila"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label (frame number)"
msgid "Nr"
msgstr "N.º"

#: plugin_kategdb.cpp:165
#, kde-format
msgctxt "Column label"
msgid "Frame"
msgstr "Chamada"

#: plugin_kategdb.cpp:197
#, kde-format
msgctxt "Tab label"
msgid "Debug Output"
msgstr "Saída de depuración"

#: plugin_kategdb.cpp:198
#, kde-format
msgctxt "Tab label"
msgid "Settings"
msgstr "Configuración"

#: plugin_kategdb.cpp:240
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"Advanced Settings -> Source file search paths"
msgstr ""
"<title>Non foi posíbel abrir o ficheiro:</title><nl/>%1<br/>Probe a engadir "
"unha ruta de busca en «Configuración avanzada → Rutas de busca dos ficheiros "
"fonte»."

#: plugin_kategdb.cpp:265
#, kde-format
msgid "Start Debugging"
msgstr "Iniciar a depuración"

#: plugin_kategdb.cpp:275
#, kde-format
msgid "Kill / Stop Debugging"
msgstr "Matar / Deter a depuración"

#: plugin_kategdb.cpp:282
#, kde-format
msgid "Continue"
msgstr "Continuar"

#: plugin_kategdb.cpp:288
#, kde-format
msgid "Toggle Breakpoint / Break"
msgstr "Conmutar o punto de parada / Parar"

#: plugin_kategdb.cpp:294
#, kde-format
msgid "Step In"
msgstr "Entrar"

#: plugin_kategdb.cpp:301
#, kde-format
msgid "Step Over"
msgstr "Pasar por riba"

#: plugin_kategdb.cpp:308
#, kde-format
msgid "Step Out"
msgstr "Saír"

#: plugin_kategdb.cpp:315 plugin_kategdb.cpp:347
#, kde-format
msgid "Run To Cursor"
msgstr "Executar ata o cursor"

#: plugin_kategdb.cpp:322
#, kde-format
msgid "Restart Debugging"
msgstr "Reiniciar a depuración"

#: plugin_kategdb.cpp:330 plugin_kategdb.cpp:349
#, kde-format
msgctxt "Move Program Counter (next execution)"
msgid "Move PC"
msgstr "Mover a PC"

#: plugin_kategdb.cpp:335
#, kde-format
msgid "Print Value"
msgstr "Amosar o valor"

#: plugin_kategdb.cpp:344
#, kde-format
msgid "popup_breakpoint"
msgstr "popup_breakpoint"

#: plugin_kategdb.cpp:346
#, kde-format
msgid "popup_run_to_cursor"
msgstr "popup_run_to_cursor"

#: plugin_kategdb.cpp:428 plugin_kategdb.cpp:444
#, kde-format
msgid "Insert breakpoint"
msgstr "Inserir un punto de parada"

#: plugin_kategdb.cpp:442
#, kde-format
msgid "Remove breakpoint"
msgstr "Retirar o punto de parada"

#: plugin_kategdb.cpp:571
#, kde-format
msgid "Execution point"
msgstr "Punto de execución"

#: plugin_kategdb.cpp:710
#, kde-format
msgid "Thread %1"
msgstr "Fío %1"

#: plugin_kategdb.cpp:810
#, kde-format
msgid "IO"
msgstr "E/S"

#: plugin_kategdb.cpp:894
#, kde-format
msgid "Breakpoint"
msgstr "Punto de parada"

#. i18n: ectx: Menu (debug)
#: ui.rc:6
#, kde-format
msgid "&Debug"
msgstr "&Depurar"

#. i18n: ectx: ToolBar (gdbplugin)
#: ui.rc:29
#, kde-format
msgid "Debug Plugin"
msgstr "Complemento de depuración"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@kde-espana.es"

#~ msgid "GDB Integration"
#~ msgstr "Integración de GDB"

#~ msgid "Kate GDB Integration"
#~ msgstr "Integración de GDB en Kate"
